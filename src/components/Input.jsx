import React from 'react';

export default (props) => (
	<div className="form-group">
		<label for={props.id}>{props.label}</label>
		<input
			type={props.type || 'text'}
			className="form-control"
			id={props.id}
			aria-describedby={props.describedby || props.placeholder}
			placeholder={props.placeholder}
		/>
	</div>
);
