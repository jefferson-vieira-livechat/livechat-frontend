import React, { Component } from 'react';

export default class Grid extends Component {
	toCssCols() {
		const cols = this.props.cols ? this.props.cols.split(' ') : [ 12 ];
		let css = '';
		if (+cols[0]) {
			css += `col-${cols[0]}`;
		}
		if (+cols[1]) {
			css += ` col-sm-${cols[1]}`;
		}
		if (+cols[2]) {
			css += ` col-md-${cols[2]}`;
		}
		if (+cols[3]) {
			css += ` col-lg-${cols[3]}`;
		}
		if (+cols[4]) {
			css += ` col-xl-${cols[4]}`;
		}
		return css;
	}

	render() {
		return <div className={this.toCssCols()}>{this.props.children}</div>;
	}
}
