import React, { Component } from 'react';

import Row from '../../components/layout/Row';
import Grid from '../../components/layout/Grid';
import Input from '../../components/Input';

import './login.css';

export default class Login extends Component {
	render() {
		return (
			<div className="container d-flex justify-content-center align-items-center">
				<Grid cols="12 6 3">
					<div className="wrapper">
						<form id="formLogin">
							<Input id="inputUser" label="Usuário" placeholder="Digite seu nome" />
						</form>
					</div>
				</Grid>
			</div>
		);
	}
}
